package com.ecobank.digx.cz.appx.RMManagement.service;


import javax.ws.rs.core.Response;

import com.ofss.digx.datatype.complex.Account;

public interface IRMDetails {

	Response fetchRMDetails(Account var1) throws Exception;

}
