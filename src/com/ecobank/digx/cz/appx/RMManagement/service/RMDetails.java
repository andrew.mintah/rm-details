package com.ecobank.digx.cz.appx.RMManagement.service;


import com.ofss.digx.app.context.ChannelContext;

import com.ofss.digx.app.core.ChannelInteraction;
import com.ofss.digx.appx.AbstractRESTApplication;
import com.ofss.digx.datatype.complex.Account;

import com.ofss.digx.infra.exceptions.Exception;
import com.ofss.fc.infra.log.impl.MultiEntityLogger;
import com.ofss.digx.service.response.BaseResponseObject;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.*;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import com.ecobank.digx.cz.app.RMManagement.dto.*;


@Path("/RMDetails")
@Tag(name = "RM Details", description = "REST API's Containing Relationship Manager Details.")
public class RMDetails extends AbstractRESTApplication implements IRMDetails {

	private static final String THIS_COMPONENT_NAME = RMDetails.class.getName();
	private MultiEntityLogger formatter = MultiEntityLogger.getUniqueInstance();
	private static transient Logger logger = MultiEntityLogger.getUniqueInstance().getLogger(THIS_COMPONENT_NAME);


	@GET
	@Produces({"application/json"})
	@Path("/{accountId}")
	public Response fetchRMDetails(@PathParam("accountId") Account accountId) throws java.lang.Exception 
		 {
		if (this.logger.isLoggable(Level.FINE)) {
			this.logger.log(Level.FINE, this.formatter.formatMessage(
					"Entered into fetchTransactions in REST %s: accountId=%s",
					new Object[]{THIS_COMPONENT_NAME, accountId, }));
		}

		ChannelContext channelContext = null;
		Response response = null;
		RMDetailsResponseDTO rmDetailsResponse = new RMDetailsResponseDTO();
		ChannelInteraction channelInteraction = ChannelInteraction.getInstance();

		try {
			channelContext = super.getChannelContext();
			channelInteraction.begin(channelContext);
		
			com.ecobank.digx.cz.app.RMManagement.service.RMDetails RMDetailsService = new com.ecobank.digx.cz.app.RMManagement.service.RMDetails();
			rmDetailsResponse  = RMDetailsService.fetchRMDetails(channelContext.getSessionContext(),
					accountId);
			response = this.buildResponse(rmDetailsResponse, Response.Status.OK);
		} catch (Exception var25) {
			this.logger.log(Level.SEVERE,
					this.formatter.formatMessage(
							"Exception encountered while invoking the service %s for accountActivityRequestDTO=%s",
							new Object[]{RMDetails.class.getName(), accountId}),
					var25);
			response = this.buildResponse(var25, Status.BAD_REQUEST);
		} finally {
			try {
				channelInteraction.close(channelContext);
			} catch (Exception var24) {
				this.logger.log(Level.SEVERE, this.formatter.formatMessage(
						"Error encountered while closing channelContext %s", new Object[]{channelContext}), var24);
				response = this.buildResponse(var24, Status.INTERNAL_SERVER_ERROR);
			}

		}

		if (this.logger.isLoggable(Level.FINE)) {
			this.logger.log(Level.FINE,
					this.formatter.formatMessage(
							"Exiting fetchTransactions of Demand Deposit REST service, accountActivityResponse: %s",
							new Object[]{rmDetailsResponse}));
		}

		return response;
	}


}
