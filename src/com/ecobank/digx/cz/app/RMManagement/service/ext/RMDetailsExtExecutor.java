package com.ecobank.digx.cz.app.RMManagement.service.ext;

import java.util.List;
import java.util.logging.Logger;
import com.ofss.digx.app.ext.ServiceExtensionFactory;
import com.ecobank.digx.cz.app.RMManagement.dto.*;



import com.ofss.digx.datatype.complex.Account;
import com.ofss.fc.app.context.SessionContext;
import com.ofss.fc.infra.log.impl.MultiEntityLogger;

public class RMDetailsExtExecutor implements IRMDetailsExtExecutor {
	
    private List<IRMDetailsExt> serviceExtensions;

	
	public void preFetchRMDetails(SessionContext sessionContext,
			Account accountId) throws Exception {
		for (int i = 0; i < this.serviceExtensions.size(); ++i) {
			((IRMDetailsExt) this.serviceExtensions.get(i)).preFetchRMDetails(sessionContext,
					accountId);
		}

	}

	public void postFetchRMDetails(SessionContext sessionContext,
			Account accountId,
			RMDetailsResponseDTO rmDetailsResponseDTO) throws Exception {
		for (int i = 0; i < this.serviceExtensions.size(); ++i) {
			((IRMDetailsExt) this.serviceExtensions.get(i)).postFetchRMDetails(sessionContext,
					accountId, rmDetailsResponseDTO);
		}

	}


}
