package com.ecobank.digx.cz.app.RMManagement.service;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.ofss.digx.annotations.Role;
import com.ofss.digx.app.AbstractApplication;
import com.ofss.digx.app.Interaction;
import com.ofss.digx.app.adapter.AdapterFactoryConfigurator;
import com.ofss.digx.app.ext.ServiceExtensionFactory;
import com.ofss.digx.datatype.complex.Account;
import com.ofss.digx.enumeration.RoleType;
import com.ofss.fc.app.adapter.IAdapterFactory;
import com.ofss.fc.app.context.SessionContext;
import com.ofss.fc.infra.log.impl.MultiEntityLogger;
import com.ofss.fc.service.response.TransactionStatus;
import com.ofss.digx.infra.exceptions.*;
import com.ofss.digx.infra.exceptions.Exception;
import com.ecobank.digx.cz.app.RMManagement.adapter.IRMDetailsAdapter;
import com.ecobank.digx.cz.app.RMManagement.dto.*;
import com.ecobank.digx.cz.app.RMManagement.service.ext.IRMDetailsExtExecutor;


public class RMDetails extends AbstractApplication implements IRMDetails {
	private static final String THIS_COMPONENT_NAME = RMDetails.class.getName();
	private IRMDetailsExtExecutor extensionExecutor = (IRMDetailsExtExecutor) ServiceExtensionFactory
			.getServiceExtensionExecutor(THIS_COMPONENT_NAME);
	private MultiEntityLogger formatter = MultiEntityLogger.getUniqueInstance();
	private transient Logger LOGGER  = this.formatter.getLogger(THIS_COMPONENT_NAME);

	
	public RMDetailsResponseDTO fetchRMDetails(SessionContext sessionContext,
			 Account accountId) throws java.lang.Exception {
		
		if (LOGGER.isLoggable(Level.FINE)) {
			LOGGER.log(Level.FINE,
					this.formatter.formatMessage("Entering fetchTransactions of %s, AccountActivityRequestDTO:%s ",
							new Object[]{THIS_COMPONENT_NAME, accountId}));
		}

		super.checkAccessPolicy("com.ecobank.digx.cz.app.RMManagement.service.RMDetails.fetchRMDetails",
				new Object[]{sessionContext, accountId});
		Interaction.begin(sessionContext);
		TransactionStatus transactionStatus = this.fetchTransactionStatus();
		RMDetailsResponseDTO rmDetailsResponseDTO = new RMDetailsResponseDTO();
	
		try {
			this.extensionExecutor.preFetchRMDetails(sessionContext, accountId);
			
		
			IAdapterFactory rmDetailsAdapterFactory = (IAdapterFactory) AdapterFactoryConfigurator.getInstance()
					.getAdapterFactory("RMDETAILS_ADAPTER_FACTORY");
			
			IRMDetailsAdapter rmDetailsAdapter = (IRMDetailsAdapter) rmDetailsAdapterFactory.getAdapter("RMDetailsAdapter");
	
			rmDetailsResponseDTO =  rmDetailsAdapter.read(accountId);
					
			this.extensionExecutor.postFetchRMDetails(sessionContext, accountId,rmDetailsResponseDTO);
			rmDetailsResponseDTO.setStatus(this.buildStatus(transactionStatus));
		} catch (Exception var31) {
			LOGGER.log(Level.SEVERE, this.formatter.formatMessage(
					"Exception has occured while getting response object inside the fetchTransactions method of %s .Exception details are %s",
					new Object[]{ RMDetails.class,var31}));
			this.fillTransactionStatus(transactionStatus, var31);
		} catch (RuntimeException var32) {
			this.fillTransactionStatus(transactionStatus, var32);
			LOGGER.log(Level.SEVERE,
					this.formatter.formatMessage(
							"RunTimeException from fetchTransactions for accountActivityRequestdto"),
					var32);
		} finally {
			Interaction.close();
		}

		super.checkResponsePolicy(sessionContext, rmDetailsResponseDTO);
		super.encodeOutput(rmDetailsResponseDTO);


		return rmDetailsResponseDTO;
	}

}
