package com.ecobank.digx.cz.app.RMManagement.service.ext;

import com.ofss.digx.datatype.complex.Account;
import com.ofss.fc.app.context.SessionContext;
import com.ecobank.digx.cz.app.RMManagement.dto.*;


public interface IRMDetailsExtExecutor {
	
	void preFetchRMDetails(SessionContext var1, Account var2) throws Exception;

	void postFetchRMDetails(SessionContext var1, Account var2,
			RMDetailsResponseDTO var3) throws Exception;


}
