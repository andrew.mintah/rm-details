package com.ecobank.digx.cz.app.RMManagement.service.ext;

import java.util.logging.Logger;
import com.ecobank.digx.cz.app.RMManagement.dto.*;


import com.ecobank.digx.cz.app.RMManagement.service.*;
import com.ofss.digx.datatype.complex.Account;
import com.ofss.fc.app.context.SessionContext;
import com.ofss.fc.infra.log.impl.MultiEntityLogger;
import com.ecobank.digx.cz.appx.RMManagement.service.*;

public interface IRMDetailsExt {

	
	void preFetchRMDetails(SessionContext var1, Account var2) throws Exception;

	void postFetchRMDetails(SessionContext var1, Account var2,
			RMDetailsResponseDTO var3) throws Exception;


}
