package com.ecobank.digx.cz.app.RMManagement.service.ext;

import com.ofss.digx.datatype.complex.Account;
import com.ofss.fc.app.context.SessionContext;
import com.ecobank.digx.cz.app.RMManagement.dto.*;

public class VoidRMDetailsExt {
	public void preFetchTransactions(SessionContext sessionContext,
			Account accountId) throws Exception {
	}

	public void postFetchTransactions(SessionContext sessionContext,
			Account accountId,
			RMDetailsResponseDTO rmDetailsResponseDTO) throws Exception {
	}

}
