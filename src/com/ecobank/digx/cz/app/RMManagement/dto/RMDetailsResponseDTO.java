package com.ecobank.digx.cz.app.RMManagement.dto;

import com.ofss.digx.service.response.BaseResponseObject;

public class RMDetailsResponseDTO extends BaseResponseObject{
	
	private String RMName;
	private String RMMobileNo;

	public String getRMName() {
		return RMName;
	}

	public void setRMName(String rMName) {
		RMName = rMName;
	}

	public String getRMMobileNo() {
		return RMMobileNo;
	}

	public void setRMMobileNo(String rMMobileNo) {
		RMMobileNo = rMMobileNo;
	}
	

}
