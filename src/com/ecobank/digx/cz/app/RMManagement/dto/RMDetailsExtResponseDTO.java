package com.ecobank.digx.cz.app.RMManagement.dto;

import com.ofss.extsystem.dto.ResponseDTO;


public class RMDetailsExtResponseDTO extends ResponseDTO {
	
	
	private static final long serialVersionUID = 1L;

	private String RMName;

	private String RMMobileNo;

	public String getRMName() {
		return RMName;
	}

	public void setRMName(String rMName) {
		RMName = rMName;
	}

	public String getRMMobileNo() {
		return RMMobileNo;
	}

	public void setRMMobileNo(String rMMobileNo) {
		RMMobileNo = rMMobileNo;
	}

}
