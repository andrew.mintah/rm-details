package com.ecobank.digx.cz.app.RMManagement.dto;

import com.ofss.extsystem.dto.RequestDTO;

public class RMDetailsRequestDTO extends RequestDTO{
	
	private String accountID;

	public String getAccountID() {
		return accountID;
	}

	public void setAccountID(String accountID) {
		this.accountID = accountID;
	}


}



