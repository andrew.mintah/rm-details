package com.ecobank.digx.cz.app.RMManagement.adapter;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.ecobank.digx.cz.app.RMManagement.dto.RMDetailsExtResponseDTO;
import com.ecobank.digx.cz.app.RMManagement.dto.RMDetailsRequestDTO;
import com.ecobank.digx.cz.app.RMManagement.dto.RMDetailsResponseDTO;
import com.ofss.digx.app.account.dto.dda.DemandDepositAccountBranchResponse;
import com.ofss.digx.app.adapter.AbstractAdapter;
import com.ofss.digx.app.core.AdapterInteraction;
import com.ofss.digx.datatype.complex.Account;
import com.ofss.digx.extxface.impl.AbstractAdapterHelper;
import com.ofss.extsystem.dto.AccountDetailsResponseDTO;
import com.ofss.extsystem.dto.HostRequestDTO;
import com.ofss.extsystem.dto.HostResponseDTO;
import com.ofss.extsystem.dto.UserContextDTO;
import com.ofss.fc.infra.log.impl.MultiEntityLogger;
import com.ofss.extsystem.business.extsystems.HostAdapterHelper;
import com.ofss.extsystem.business.extsystems.HostAdapterManager;

public class RMDetailsAdapter extends AbstractAdapter implements IRMDetailsAdapter {
	
	private static final String THIS_COMPONENT_NAME = RMDetailsAdapter.class.getName();
	private static final MultiEntityLogger formatter = MultiEntityLogger.getUniqueInstance();
	private static final Logger logger = formatter.getLogger(THIS_COMPONENT_NAME);

	
	
	public RMDetailsResponseDTO read(Account accountId) throws Exception {
		if (logger.isLoggable(Level.FINE)) {
			logger.log(Level.FINE,
					formatter.formatMessage(
							"Entered in method read of %s used to read RM Name and number details, accountId = %s",
							new Object[]{THIS_COMPONENT_NAME, accountId}));
		}

		super.checkRequest("com.ecobank.digx.cz.app.RMManagement.adapter.read",
				new Object[]{accountId});
		AdapterInteraction.begin();
		RMDetailsResponseDTO responseDTO = null;
		RMDetailsRequestDTO requestDTO = new RMDetailsRequestDTO();
		RMDetailsExtResponseDTO adapterResponse = null;
		HostRequestDTO hostRequest = new HostRequestDTO();
		HostResponseDTO hostResponse = new HostResponseDTO();

		try {
			
			AbstractAdapterHelper helper = AbstractAdapterHelper.getInstance();
			requestDTO.userContext = new UserContextDTO();
			requestDTO.userContext.idEntity = "B001";
			requestDTO.userContext.idTxn = "RMF";
			requestDTO.userContext.idRequest = "RM_FETCH";
			requestDTO.userContext.serviceVersion = 0;
			requestDTO.userContext.refIdEntity = "B001";
			requestDTO.userContext.userType = "EN1";
			requestDTO.setAccountID(accountId.getValue()); 

			
			hostRequest = HostAdapterHelper.buildHostRequest(requestDTO);
			hostResponse = HostAdapterManager.processRequest(hostRequest);
		} catch (java.lang.Exception var10) {
			logger.log(Level.SEVERE, formatter.formatMessage(
					" Exception has occured while getting response object of %s inside the read method of %s for %s. Exception details are %s",
					new Object[]{RMDetailsResponseDTO.class.getName(), THIS_COMPONENT_NAME, accountId,
							var10}),
					var10);
		} finally {
			AdapterInteraction.close();
		}

	    
		 adapterResponse =  (RMDetailsExtResponseDTO)hostResponse.response;
		 if (adapterResponse != null) {
		        
			 	responseDTO = new RMDetailsResponseDTO();
			 	responseDTO.setRMName(adapterResponse.getRMName());
			 	responseDTO.setRMMobileNo(adapterResponse.getRMMobileNo());
		 		
	 } 
		 
		super.checkResponse(responseDTO);
		if (logger.isLoggable(Level.FINE)) {
			logger.log(Level.FINE, formatter.formatMessage(
					"Exiting in method read of %s used to read RM Name and number details, responseDTO  = %s",
					new Object[]{THIS_COMPONENT_NAME, responseDTO}));
		}

		return responseDTO;
	}

}
