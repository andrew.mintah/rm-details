package com.ecobank.digx.cz.app.RMManagement.adapter;

import com.ecobank.digx.cz.app.RMManagement.dto.RMDetailsResponseDTO;
import com.ofss.digx.datatype.complex.Account;

public interface IRMDetailsAdapter {
	
	RMDetailsResponseDTO read(Account var1) throws Exception;
	



}
