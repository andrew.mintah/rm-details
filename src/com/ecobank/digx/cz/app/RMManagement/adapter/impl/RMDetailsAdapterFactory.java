package com.ecobank.digx.cz.app.RMManagement.adapter.impl;

import com.ecobank.digx.cz.app.RMManagement.adapter.IRMDetailsAdapter;
import com.ecobank.digx.cz.app.RMManagement.adapter.RMDetailsAdapter;
import com.ofss.digx.app.account.adapter.impl.AccountAdapterFactory;
import com.ofss.digx.app.adapter.AdapterFactory;
import com.ofss.digx.datatype.NameValuePair;

public class RMDetailsAdapterFactory extends AdapterFactory{
	private static RMDetailsAdapterFactory adapterFactory = null;

	public static RMDetailsAdapterFactory getInstance() {
		     if (adapterFactory == null) {
		       synchronized (AccountAdapterFactory.class) {
		         adapterFactory = new RMDetailsAdapterFactory();
		       } 
		     }
		     return adapterFactory;
		  }
	public IRMDetailsAdapter getAdapter(String adapterClass) {
		IRMDetailsAdapter adapter = null;
		if (adapterClass.equals("RMDetailsAdapter")) {
			adapter = new RMDetailsAdapter();
		}

		return adapter;
	}

	public Object getAdapter(String adapter, NameValuePair[] nameValues) {
		return null;
	}

}
