package com.ecobank.cz.extsystem.business.extsystems.adapters;

import java.sql.Connection;
import java.util.ArrayList;

import com.ecobank.digx.cz.app.RMManagement.dto.RMDetailsExtResponseDTO;
import com.ecobank.digx.cz.app.RMManagement.dto.RMDetailsRequestDTO;
import com.ecobank.digx.cz.app.RMManagement.dto.RMDetailsResponseDTO;
import com.ofss.extsystem.business.extsystems.HostAdapter;
import com.ofss.extsystem.business.extsystems.HostAdapterConstants;
import com.ofss.extsystem.business.extsystems.HostAdapterHelper;
import com.ofss.extsystem.business.utils.ServiceUtils;
import com.ofss.extsystem.dto.HostRequestDTO;
import com.ofss.extsystem.dto.HostResponseDTO;
import com.ofss.extsystem.framework.utils.dbaccess.JDBCEngine;
import com.ofss.extsystem.framework.utils.dbaccess.JDBCResultSet;

public class RMDetailsExtAdapter implements HostAdapter, HostAdapterConstants {
	
	private static final String SEL_RM_FOR_ACCOUNT = 
			"select code_desc from fcc_gltms_mis_code a,fcc_mitbs_class_mapping b where a.mis_code = b.comp_mis_1 and mis_class='ACC_OFCR' and unit_ref_no = ?";

	public HostResponseDTO processRequest(HostRequestDTO p_request) throws Exception {
		ArrayList<String> l_args = null;
		RMDetailsRequestDTO l_rmdet_request = null;
		RMDetailsExtResponseDTO l_rmdet_response = null;
		HostResponseDTO l_host_response = null;
		JDBCResultSet l_rs = null;
		Connection l_con = null;

		try {
			l_args = new ArrayList();
			l_rmdet_request = (RMDetailsRequestDTO) p_request.hostRequest;
			l_con = p_request.txnContext.getConnection("A1");
			l_host_response = HostAdapterHelper.copyRequestToResponse(p_request);
			l_args.add(l_rmdet_request.getAccountID());
			l_rs = 	JDBCEngine.executeQuery(SEL_RM_FOR_ACCOUNT,
					l_args.size(), l_args, l_con);
			l_rmdet_response = new RMDetailsExtResponseDTO();
			if (!l_rs.next()) {
				ServiceUtils.addErrorToResponse(l_rmdet_request, l_rmdet_response, ServiceUtils.getError("10001",
						l_rmdet_request.userContext.idLang, l_rmdet_request.userContext.idDevice));
				l_rmdet_response.result.returnCode = 1;
				l_host_response.hostResponse = l_rmdet_response;
				HostResponseDTO var8 = l_host_response;
				return var8;
			}

			l_rmdet_response.setRMName(l_rs.getString("cod_desc"));
			l_rmdet_response.setRMMobileNo("3225");
		} finally {
			l_con.close();
		}

		l_rmdet_response.result.returnCode = 0;
		l_host_response.hostResponse = l_rmdet_response;
		return l_host_response;
	}

}
